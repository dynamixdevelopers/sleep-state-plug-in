/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.zeo;

import java.util.Date;

import org.ambientdynamix.contextplugins.zeo.ZeoDataContract.SleepRecord;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents a Zeo sleep record.
 * 
 * @author Darren Carlson
 * 
 */
public class ZeoSleepRecord implements Parcelable, IZeoSleepRecord {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<ZeoSleepRecord> CREATOR = new Parcelable.Creator<ZeoSleepRecord>() {
		public ZeoSleepRecord createFromParcel(Parcel in) {
			return new ZeoSleepRecord(in);
		}

		public ZeoSleepRecord[] newArray(int size) {
			return new ZeoSleepRecord[size];
		}
	};
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private long localizedStartOfNight;
	private long startOfNight;
	private long endOfNight;
	private String timeZone;
	private int zqScore;
	private int awakenings;
	private int timeInDeep;
	private int timeInLight;
	private int timeInRem;
	private int timeInWake;
	private int timeToWake;
	private int timeToZ;
	private int source;
	private int endReason;
	private byte[] baseHypnogram;
	private byte[] displayHypnogram;
	// Public data
	/** Zeo recording of sleep record concluded normally. */
	public static final int END_REASON_COMPLETE = 0;
	/** Headband is currently still actively recording sleep. */
	public static final int END_REASON_ACTIVE = 1;
	/** The battery died in the headband which stopped recording. */
	public static final int END_REASON_BATTERY_DIED = 2;
	/** Headband was disconnected from the Android device. */
	public static final int END_REASON_DISCONNECTED = 3;
	/** The Android service that interfaces with headband was killed by Android. */
	public static final int END_REASON_SERVICE_KILLED = 4;
	/** Sleep stage is unknown. */
	public static final byte SLEEP_STAGE_UNDEFINED = 0;
	/** User is awake. */
	public static final byte SLEEP_STAGE_WAKE = 1;
	/** User is in REM sleep. */
	public static final byte SLEEP_STAGE_REM = 2;
	/** User is in light sleep. */
	public static final byte SLEEP_STAGE_LIGHT = 3;
	/** User is in deep sleep. */
	public static final byte SLEEP_STAGE_DEEP = 4;
	/** Number of sleep stages defined. */
	public static final int SLEEP_STAGE_NMAX = 5;

	/**
	 * Constructs a ZeoSleepRecord from the Cursor.
	 */
	protected ZeoSleepRecord(Cursor cursor) {
		this.localizedStartOfNight = cursor.getLong(cursor.getColumnIndex(SleepRecord.LOCALIZED_START_OF_NIGHT));
		this.startOfNight = cursor.getLong(cursor.getColumnIndex(SleepRecord.START_OF_NIGHT));
		this.endOfNight = cursor.getLong(cursor.getColumnIndex(SleepRecord.END_OF_NIGHT));
		this.timeZone = cursor.getString(cursor.getColumnIndex(SleepRecord.TIMEZONE));
		this.zqScore = cursor.getInt(cursor.getColumnIndex(SleepRecord.ZQ_SCORE));
		this.awakenings = cursor.getInt(cursor.getColumnIndex(SleepRecord.AWAKENINGS));
		this.timeInDeep = cursor.getInt(cursor.getColumnIndex(SleepRecord.TIME_IN_DEEP));
		this.timeInLight = cursor.getInt(cursor.getColumnIndex(SleepRecord.TIME_IN_LIGHT));
		this.timeInRem = cursor.getInt(cursor.getColumnIndex(SleepRecord.TIME_IN_REM));
		this.timeInWake = cursor.getInt(cursor.getColumnIndex(SleepRecord.TIME_IN_WAKE));
		this.timeToWake = cursor.getInt(cursor.getColumnIndex(SleepRecord.TIME_TO_Z));
		this.timeToZ = cursor.getInt(cursor.getColumnIndex(SleepRecord.TOTAL_Z));
		this.source = cursor.getInt(cursor.getColumnIndex(SleepRecord.SOURCE));
		this.endReason = cursor.getInt(cursor.getColumnIndex(SleepRecord.END_REASON));
		this.baseHypnogram = displayHypnogram = cursor.getBlob(cursor.getColumnIndex(SleepRecord.DISPLAY_HYPNOGRAM));
		this.displayHypnogram = cursor.getBlob(cursor.getColumnIndex(SleepRecord.BASE_HYPNOGRAM));
	}

	/**
	 * {@inheritDoc}
	 */
	public long getLocalizedStartOfNight() {
		return localizedStartOfNight;
	}

	/**
	 * {@inheritDoc}
	 */
	public Date getLocalizedStartOfNightDate() {
		return new Date(localizedStartOfNight);
	}

	/**
	 * {@inheritDoc}
	 */
	public long getStartOfNight() {
		return startOfNight;
	}

	/**
	 * {@inheritDoc}
	 */
	public Date getStartOfNightDate() {
		return new Date(startOfNight);
	}

	/**
	 * {@inheritDoc}
	 */
	public long getEndOfNight() {
		return endOfNight;
	}

	/**
	 * {@inheritDoc}
	 */
	public Date getEndOfNightDate() {
		return new Date(endOfNight);
	}

	/**
	 * {@inheritDoc}
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getZqScore() {
		return zqScore;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getAwakenings() {
		return awakenings;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getTimeInDeep() {
		return timeInDeep;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getTimeInLight() {
		return timeInLight;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getTimeInRem() {
		return timeInRem;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getTimeInWake() {
		return timeInWake;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getTimeToWake() {
		return timeToWake;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getTimeToZ() {
		return timeToZ;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getSource() {
		return source;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isSourceRemote() {
		return source == SleepRecord.DATA_SOURCE_REMOTE;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isSourceLocal() {
		return source == SleepRecord.DATA_SOURCE_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getEndReason() {
		return endReason;
	}

	/**
	 * {@inheritDoc}
	 */
	public byte[] getBaseHypnogram() {
		return baseHypnogram;
	}

	/**
	 * {@inheritDoc}
	 */
	public String[] getBaseHypnogramStrings() {
		String[] strings = new String[baseHypnogram.length];
		for (int i = 0; i < baseHypnogram.length; i++) {
			strings[i] = Byte.toString(baseHypnogram[i]);
		}
		return strings;
	}

	/**
	 * {@inheritDoc}
	 */
	public byte[] getDisplayHypnogram() {
		return displayHypnogram;
	}

	/**
	 * {@inheritDoc}
	 */
	public String[] getDisplayHypnogramStrings() {
		String[] strings = new String[displayHypnogram.length];
		for (int i = 0; i < displayHypnogram.length; i++) {
			strings[i] = Byte.toString(displayHypnogram[i]);
		}
		return strings;
	}

	/**
	 * Used internally for Parcelable.
	 */
	private ZeoSleepRecord(Parcel in) {
		this.localizedStartOfNight = in.readLong();
		this.startOfNight = in.readLong();
		this.endOfNight = in.readLong();
		this.timeZone = in.readString();
		this.zqScore = in.readInt();
		this.awakenings = in.readInt();
		this.timeInDeep = in.readInt();
		this.timeInLight = in.readInt();
		this.timeInRem = in.readInt();
		this.timeInWake = in.readInt();
		this.timeToWake = in.readInt();
		this.timeToZ = in.readInt();
		this.source = in.readInt();
		this.endReason = in.readInt();
		this.baseHypnogram = new byte[in.readInt()];
		in.readByteArray(this.baseHypnogram);
		this.displayHypnogram = new byte[in.readInt()];
		in.readByteArray(this.displayHypnogram);
	}

	/**
	 * Used internally for Parcelable.
	 */
	@Override
	public void writeToParcel(Parcel out, int arg1) {
		out.writeLong(this.localizedStartOfNight);
		out.writeLong(this.startOfNight);
		out.writeLong(this.endOfNight);
		out.writeString(this.timeZone);
		out.writeInt(this.zqScore);
		out.writeInt(this.awakenings);
		out.writeInt(this.timeInDeep);
		out.writeInt(this.timeInLight);
		out.writeInt(this.timeInRem);
		out.writeInt(this.timeInWake);
		out.writeInt(this.timeToWake);
		out.writeInt(this.timeToZ);
		out.writeInt(this.source);
		out.writeInt(this.endReason);
		out.writeInt(this.baseHypnogram.length);
		out.writeByteArray(this.baseHypnogram);
		out.writeInt(this.displayHypnogram.length);
		out.writeByteArray(this.displayHypnogram);
	}

	/**
	 * Used internally for Parcelable.
	 */
	@Override
	public int describeContents() {
		return 0;
	}
}
