/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.zeo;

import org.ambientdynamix.api.application.IContextInfo;

/**
 * Represents the headband status of a Zeo mobile device.
 * 
 * @author Darren Carlson
 * 
 */
interface IZeoHeadbandStatus extends IContextInfo {
	/**
	 * The current mode of the sleep algorithm running on headband.
	 * 
	 * @return -1 = undefined mode; 0 = mode idle; 1 = tentative active (starting up); 2 = actively recording sleep; 3 =
	 *         tentative idle (shutting down).
	 * 
	 */
	public int getAlgorithmMode();

	/**
	 * String representation bluetooth 48 bit address in the form of: 11:22:33:44:55:66
	 * 
	 * @return 6 byte string in the form 11:22:33:44:55:66.
	 */
	public String getBluetoothAddress();

	/**
	 * Bluetooth name shown to the user.
	 */
	public String getBluetoothFriendlyName();

	/**
	 * Returns true if the headband is bonded to the Android device; false otherwise.
	 */
	public boolean getBonded();

	/**
	 * Number of milliseconds offset between Android device's notion of time versus the headband's.
	 * 
	 * @return milliseconds.
	 */
	public long getClockOffset();

	/**
	 * Returns true if the headband is current connected to the Android device; false otherwise.
	 */
	public boolean getConnected();

	/**
	 * Returns true if the headband is docked on the charger; false otherwise.
	 */
	public boolean getDocked();

	/**
	 * Returns true if the headband is on the user's head; false otherwise.
	 */
	public boolean getOnHead();

	/**
	 * Returns the version of the firmware running on the headband.
	 */
	public String getSoftwareVersion();
}