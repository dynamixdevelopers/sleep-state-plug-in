/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.zeo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.os.Parcel;
import android.os.Parcelable;

class ZeoSleepRecords implements IZeoSleepRecords {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<ZeoSleepRecords> CREATOR = new Parcelable.Creator<ZeoSleepRecords>() {
		public ZeoSleepRecords createFromParcel(Parcel in) {
			return new ZeoSleepRecords(in);
		}

		public ZeoSleepRecords[] newArray(int size) {
			return new ZeoSleepRecords[size];
		}
	};
	private final String TAG = this.getClass().getSimpleName();
	// Private data
	private List<ZeoSleepRecord> zeoSleepRecords = new ArrayList<ZeoSleepRecord>();;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ZeoSleepRecord> getZeoSleepRecords() {
		return zeoSleepRecords;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextType() {
		return "org.ambientdynamix.contextplugins.zeo.sleep.records";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringRepresentation(String format) {
		return "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		return formats;
	};

	public ZeoSleepRecords(List<ZeoSleepRecord> zeoSleepRecords) {
		this.zeoSleepRecords = zeoSleepRecords;
	}

	private ZeoSleepRecords(final Parcel in) {
		readFromParcel(in);
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeList(this.zeoSleepRecords);
	}

	public void readFromParcel(Parcel in) {
		in.readList(zeoSleepRecords, this.getClass().getClassLoader());
	}
}
