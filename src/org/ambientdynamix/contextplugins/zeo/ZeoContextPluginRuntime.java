/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.zeo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.ReactiveContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.api.contextplugin.security.SecuredContextInfo;
import org.ambientdynamix.contextplugins.zeo.ZeoDataContract.Headband;
import org.ambientdynamix.contextplugins.zeo.ZeoDataContract.SleepRecord;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

/**
 * Extracts context information from a Zeo mobile headband using the Zeo mobile app. Based on code from
 * https://github.com/zeoeng/zeo-android-api
 * 
 * @author Darren Carlson
 */
public class ZeoContextPluginRuntime extends ReactiveContextPluginRuntime {
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private Context c;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(PowerScheme scheme, ContextPluginSettings settings) throws Exception {
		this.setPowerScheme(scheme);
		this.c = getSecuredContext();
		// Make sure we can access the Zeo Manager
		String[] projection = new String[] { Headband.SW_VERSION };
		final Cursor cursor = c.getContentResolver().query(Headband.CONTENT_URI, projection, null, null, null);
		if (cursor != null) {
			Log.i(TAG, "Zeo Manager Found");
		} else {
			Log.w(TAG, "Zeo Manager Not found");
			throw new Exception("Zeo Manager Not Found!");
		}
		cursor.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateSettings(ContextPluginSettings settings) {
		// Not supported
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() {
		/*
		 * Wait for context requests.
		 */
		Log.i(TAG, "Started");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() {
		/*
		 * At this point, the plug-in should cancel any ongoing context requests, if there are any.
		 */
		Log.d(TAG, "Stopped!");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		/*
		 * At this point, the plug-in should release any resources.
		 */
		stop();
		Log.i(TAG, "Destroyed!");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPowerScheme(PowerScheme scheme) {
		// Not supported
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void handleContextRequest(UUID requestId, String contextDataType) {
		Log.i(TAG, "handleContextRequest for requestId: " + requestId);
		if (contextDataType.equalsIgnoreCase("org.ambientdynamix.contextplugins.zeo.sleep.records")) {
			String[] projection = new String[] { SleepRecord.LOCALIZED_START_OF_NIGHT, SleepRecord.START_OF_NIGHT,
					SleepRecord.END_OF_NIGHT, SleepRecord.TIMEZONE, SleepRecord.ZQ_SCORE, SleepRecord.AWAKENINGS,
					SleepRecord.TIME_IN_DEEP, SleepRecord.TIME_IN_LIGHT, SleepRecord.TIME_IN_REM,
					SleepRecord.TIME_IN_WAKE, SleepRecord.TIME_TO_Z, SleepRecord.TOTAL_Z, SleepRecord.SOURCE,
					SleepRecord.END_REASON, SleepRecord.DISPLAY_HYPNOGRAM, SleepRecord.BASE_HYPNOGRAM };
			final Cursor cursor = c.getContentResolver().query(SleepRecord.CONTENT_URI, projection, null, null, null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					List<ZeoSleepRecord> records = new ArrayList<ZeoSleepRecord>();
					do {
						records.add(new ZeoSleepRecord(cursor));
					} while (cursor.moveToNext());
					sendContextEvent(requestId, new SecuredContextInfo(new ZeoSleepRecords(records),
							PrivacyRiskLevel.MAX, PluginConstants.JSON_WEB_ENCODING));
				} else {
					Log.w(TAG, "No sleep records found.");
					sendContextRequestError(requestId, "No sleep records found", ErrorCodes.INTERNAL_PLUG_IN_ERROR);
				}
			} else {
				Log.w(TAG, "Could not access Zeo Manager");
				sendContextRequestError(requestId, "Could not access Zeo Manager - is it installed?",
						ErrorCodes.INTERNAL_PLUG_IN_ERROR);
			}
			cursor.close();
		} else if (contextDataType.equalsIgnoreCase("org.ambientdynamix.contextplugins.zeo.headband.status")) {
			String[] projection = new String[] { Headband.ALGORITHM_MODE, Headband.BLUETOOTH_ADDRESS,
					Headband.BLUETOOTH_FRIENDLY_NAME, Headband.BONDED, Headband.CLOCK_OFFSET, Headband.CONNECTED,
					Headband.DOCKED, Headband.ON_HEAD, Headband.SW_VERSION };
			final Cursor cursor = c.getContentResolver().query(Headband.CONTENT_URI, projection, null, null, null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					sendContextEvent(requestId, new SecuredContextInfo(new ZeoHeadbandStatus(cursor),
							PrivacyRiskLevel.MAX));
				} else {
					Log.w(TAG, "No sleep records found.");
					sendContextRequestError(requestId, "No sleep records found", ErrorCodes.INTERNAL_PLUG_IN_ERROR);
				}
			} else {
				Log.w(TAG, "Could not access Zeo Manager");
				sendContextRequestError(requestId, "Could not access Zeo Manager - is it installed?",
						ErrorCodes.INTERNAL_PLUG_IN_ERROR);
			}
			cursor.close();
		} else {
			Log.w(TAG, "Context type not supported: " + contextDataType);
			sendContextRequestError(requestId, "Context type not supported: " + contextDataType,
					ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void handleConfiguredContextRequest(UUID requestId, String contextInfoType, Bundle scanConfig) {
		handleContextRequest(requestId, contextInfoType);
	}
}