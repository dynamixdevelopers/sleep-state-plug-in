/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.zeo;

import java.util.HashSet;
import java.util.Set;

import org.ambientdynamix.api.application.IContextInfo;
import org.ambientdynamix.contextplugins.zeo.ZeoDataContract.Headband;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents the status of a Zeo headband.
 * 
 * @author Darren Carlson
 * 
 */
class ZeoHeadbandStatus implements IContextInfo, IZeoHeadbandStatus {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<ZeoHeadbandStatus> CREATOR = new Parcelable.Creator<ZeoHeadbandStatus>() {
		public ZeoHeadbandStatus createFromParcel(Parcel in) {
			return new ZeoHeadbandStatus(in);
		}

		public ZeoHeadbandStatus[] newArray(int size) {
			return new ZeoHeadbandStatus[size];
		}
	};
	// Public data
	/** Algorithm in an undefined state (not known) */
	public static final int ALGO_MODE_UNDEFINED = -1;
	/** Sleep algorithm is idle. */
	public static final int ALGO_MODE_IDLE = 0;
	/** Sleep algorithm is tentative active (starting up) */
	public static final int ALGO_MODE_TENTATIVE_ACTIVE = 1;
	/** Sleep algorithm is actively recording sleep. */
	public static final int ALGO_MODE_ACTIVE = 2;
	/** Sleep algorithm is tentative idle (shutting down) */
	public static final int ALGO_MODE_TENTATIVE_IDLE = 3;
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private int algorithmMode;
	private String bluetoothAddress;
	private String bluetoothFriendlyName;
	private String bonded;
	private long clockOffset;
	private String connected;
	private String docked;
	private String onHead;
	private String softwareVersion;

	/**
	 * Creates a ZeoHeadbandStatus.
	 */
	public ZeoHeadbandStatus(Cursor cursor) {
		this.algorithmMode = cursor.getInt(cursor.getColumnIndex(Headband.ALGORITHM_MODE));
		this.bluetoothAddress = cursor.getString(cursor.getColumnIndex(Headband.BLUETOOTH_ADDRESS));
		this.bluetoothFriendlyName = cursor.getString(cursor.getColumnIndex(Headband.BLUETOOTH_FRIENDLY_NAME));
		this.bonded = cursor.getString(cursor.getColumnIndex(Headband.BONDED));
		this.clockOffset = cursor.getLong(cursor.getColumnIndex(Headband.CLOCK_OFFSET));
		this.connected = cursor.getString(cursor.getColumnIndex(Headband.CONNECTED));
		this.docked = cursor.getString(cursor.getColumnIndex(Headband.DOCKED));
		this.onHead = cursor.getString(cursor.getColumnIndex(Headband.ON_HEAD));
		this.softwareVersion = cursor.getString(cursor.getColumnIndex(Headband.SW_VERSION));
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextType() {
		return "org.ambientdynamix.contextplugins.zeo.headband.status";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return "Zeo Headband Status - algorithmMode=" + getAlgorithmMode() + "&" + "bluetoothAddress=" + getBluetoothAddress() + "&"
					+ "bluetoothFriendlyName=" + getBluetoothFriendlyName() + "&" + "bonded=" + getBonded() + "&"
					+ "clockOffset=" + getClockOffset() + "&" + "connected=" + getConnected() + "&" + "docked="
					+ getDocked() + "&" + "onHead=" + getOnHead() + "&" + "softwareVersion=" + getSoftwareVersion();
		else
			return "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	};

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	public int getAlgorithmMode() {
		return algorithmMode;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getBluetoothAddress() {
		return bluetoothAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getBluetoothFriendlyName() {
		return bluetoothFriendlyName;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean getBonded() {
		return parseBoolean(bonded);
	}

	/**
	 * {@inheritDoc}
	 */
	public long getClockOffset() {
		return clockOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean getConnected() {
		return parseBoolean(connected);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean getDocked() {
		return parseBoolean(docked);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean getOnHead() {
		return parseBoolean(onHead);
	}

	/**
	 * {@inheritDoc}
	 */
	public String getSoftwareVersion() {
		return softwareVersion;
	}

	/*
	 * Utility for parsing boolean strings.
	 */
	private boolean parseBoolean(String bs) {
		bs = bs.trim();
		if (bs.equalsIgnoreCase("true") || bs.equalsIgnoreCase("1") || bs.equalsIgnoreCase("t"))
			return true;
		else
			return false;
	}

	/**
	 * Used internally for Parcelable.
	 */
	private ZeoHeadbandStatus(final Parcel in) {
		this.algorithmMode = in.readInt();
		this.bluetoothAddress = in.readString();
		this.bluetoothFriendlyName = in.readString();
		this.bonded = in.readString();
		this.clockOffset = in.readLong();
		this.connected = in.readString();
		this.docked = in.readString();
		this.onHead = in.readString();
		this.softwareVersion = in.readString();
	}

	/**
	 * Used internally for Parcelable.
	 */
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(this.algorithmMode);
		out.writeString(this.bluetoothAddress);
		out.writeString(this.bluetoothFriendlyName);
		out.writeString(this.bonded);
		out.writeLong(this.clockOffset);
		out.writeString(this.connected);
		out.writeString(this.docked);
		out.writeString(this.onHead);
		out.writeString(this.softwareVersion);
	}

	/**
	 * Used internally for Parcelable.
	 */
	public int describeContents() {
		return 0;
	}
}
